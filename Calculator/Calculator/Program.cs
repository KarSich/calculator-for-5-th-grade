﻿using System;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;

namespace Calculator
{
    class Program
    {
        static void MainMenu()
        {
        }
        static void FromSystemToSystem()
        {

        }
        static void Main(string[] args)
        {
            Console.WriteLine("Вас приветствует обучающий калькулятор систем счисления! ");
            Console.WriteLine("Для выбора пункта меню введите его номер. Для выхода нажмите клавишу Esc");
            List<string> menuList = new List<string> { "1. Перевод из одной системы счисления в другую",
                                                       "2. Перевод с римскими числами",
                                                       "3. Калькулятор", };
            foreach (string menuItem in menuList)
                Console.WriteLine(menuItem);
            ConsoleKeyInfo key = Console.ReadKey(true);
            if (key.Key == ConsoleKey.D1)
                Console.WriteLine("FromSystemToSystem");
            else if (key.Key == ConsoleKey.D2)
                Console.WriteLine("Roman Numbers And Magic");
            else if (key.Key == ConsoleKey.D3)
                Console.WriteLine("Real Calculator");
            else if (key.Key == ConsoleKey.Escape)
                Console.WriteLine("Exiting Program ...");
            else Console.WriteLine("Введен неверный символ");
        }
    }
}
